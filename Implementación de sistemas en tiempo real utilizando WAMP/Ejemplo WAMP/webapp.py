import asyncio
import logging

from aiohttp import web
from aiohttp.web_exceptions import HTTPOk, HTTPInternalServerError
from autobahn.asyncio.component import Component



# Setup logging
logger = logging.getLogger(__name__)

RESOURCES_PATH = "./webapp"

ROUTES = web.RouteTableDef()



ROUTES.static("/", "./{}/".format(RESOURCES_PATH))
INDEX = open('{}/index.html'.format(RESOURCES_PATH)).read()

class WebApplication(object):
    """
    A simple Web application that publishes an event every time the
    url "/" is visited.
    """

    count = 0

    def __init__(self, app, wamp_comp):
        self._app = app
        self._wamp = wamp_comp
        self._session = None  # "None" while we're disconnected from WAMP router

        # associate ourselves with WAMP session lifecycle
        self._wamp.on('join', self._initialize)
        self._wamp.on('leave', self._uninitialize)

        self._app.router.add_get('/', self._render_slash)
        self._app.add_routes(ROUTES)

    def _initialize(self, session, details):
        logger.info("Connected to WAMP router (session: %s, details: %s)", session, details)
        self._session = session

    def _uninitialize(self, session, reason):
        logger.warning("Lost WAMP connection (session: %s, reason: %s)", session, reason)
        self._session = None

    async def _render_slash(self, request):

        if self._session is None:
            return HTTPInternalServerError(reason="No WAMP session")
        
        self.count += 1
        
        self._session.publish(u"com.myapp.request_served", self.count, count=self.count)
        
        return web.Response(
            text=INDEX.format(self.count),
            content_type='text/html')



def main():
    REALM = "crossbardemo"
    BROKER_URI = "ws://localhost:8080/ws"
    BIND_ADDR = "0.0.0.0"
    BIND_PORT = 8081

    logging.basicConfig(
        level='DEBUG',
        format='[%(asctime)s %(levelname)s %(name)s:%(lineno)d]: %(message)s')

    logger.info("Starting aiohttp backend at %s:%s...", BIND_ADDR, BIND_PORT)
    loop = asyncio.get_event_loop()

    component = Component(
        transports=BROKER_URI,
        realm=REALM,
    )
    component.start(loop=loop)

    # When not using run() we also must start logging ourselves.
    import txaio
    txaio.start_logging(level='info')

    app = web.Application()

    _ = WebApplication(app, component)

    web.run_app(app, host=BIND_ADDR, port=BIND_PORT, loop=loop)


if __name__ == '__main__':
    main()