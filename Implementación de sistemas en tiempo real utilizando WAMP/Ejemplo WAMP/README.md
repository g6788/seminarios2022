<h1>¿Cómo ejecutar?</h1>

1. Necesitamos a Crossbar corriendo, para levantarlo necesitamos tener Docker instalado, estar dentro de la carpeta del proyecto y correr desde la terminal el siguiente comando:

<center><code> ./start-crossbar.sh </code></center>



**Importante:** Tenemos que tener instalados los siguientes módulos:

* *pip3 install autobahn[asyncio,encryption,serialization,xbr]* 
* *pip3 install numpy*
* *pip3 install aiohttp*


2. Iniciar en la terminal *webapp.py*, la cual iniciará el servicio.

3. Iniciar en la terminal el *engine.py*, el cual se encargará de publicar valores de temperatura cada cierto tiempo.

4. Dirigirse al navegador y buscar [localhost:8081](http://localhost:8081)

<h2>Parametros del motor</h2>

Podemos modificar los siguientes parametros del motor simulado desde la consola del navegador <code>F12</code>:

* Encender el ventilador del motor <code>startFan()</code>.
* Cambiar temperatura de alarma del motor <code>setMaxTemp(temp)</code>.
* Cambiar temperatura de apagado automatico del ventilador <code>setMinTemp(temp)</code>.
* Solicitar estado del ventilador <code>get(temp)</code>.

*Estas funciones realizan una llamada de ejecución remota (<code>rRPC</code>) esperando el retorno de cierto payload. Crossbar.io (Router) redireccionara la llamada que proviene desde un cliente (Caller) hacia otro cliente (Callie) que posee dicho proceso.*