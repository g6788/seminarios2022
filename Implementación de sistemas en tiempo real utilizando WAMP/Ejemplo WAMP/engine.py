import asyncio
from os import environ
from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner

import numpy.random as random

class Component(ApplicationSession):

    async def onJoin(self, details):

        self.temp = 36
        self.min_temp = 20
        self.max_temp = 100

        self.can_increase = True

        self.register(self.start_fan, "com.myapp.engine1.start_fan")
        self.register(self.fan_status, "com.myapp.engine1.get_fan_status")
        self.register(self.set_min_temp, "com.myapp.engine1.set_min_temp")
        self.register(self.set_max_temp, "com.myapp.engine1.set_max_temp")
        self.register(self.get_max_temp, "com.myapp.engine1.get_max_temp")

        loop = asyncio.get_event_loop()

        loop.run_in_executor(await self.temperature_control())


    def onDisconnect(self):
        asyncio.get_event_loop().stop()


    async def temperature_control(self):

        while True:

            if self.can_increase:
                await asyncio.sleep(random.exponential(1.1))
                self.temp = self.temp + random.normal(loc=1)
            else:
                await asyncio.sleep(random.exponential(0.9))
                self.temp = self.temp - random.normal(loc=3)

            if self.temp < self.min_temp and not self.can_increase:
                self.can_increase = True
                self.publish('com.myapp.engine1.fan_status', (not self.can_increase))
            
            print("Publishing on 'com.myapp.engine1.temp' temp: {}".format(self.temp))
            self.publish(u"com.myapp.engine1.temp", self.temp) 


    async def start_fan(self):

        if not self.can_increase:
            return False

        print("Called 'com.myapp.engine1.start_fan'")

        self.can_increase = False
        self.publish('com.myapp.engine1.fan_status', (not self.can_increase))
        return True

    async def fan_status(self):
        self.publish('com.myapp.engine1.fan_status', (not self.can_increase))
        return None

    async def set_min_temp(self, min_temp):
        self.min_temp = min_temp
        return True

    async def set_max_temp(self, max_temp):
        self.max_temp = max_temp
        self.publish('com.myapp.engine1.max_temp', self.max_temp)
        return True

    async def get_max_temp(self):
        self.publish('com.myapp.engine1.max_temp', self.max_temp)
        return True


if __name__ == '__main__':
    url = environ.get("AUTOBAHN_DEMO_ROUTER", "ws://127.0.0.1:8080/ws")
    realm = "crossbardemo"
    runner = ApplicationRunner(url, realm)
    runner.run(Component)