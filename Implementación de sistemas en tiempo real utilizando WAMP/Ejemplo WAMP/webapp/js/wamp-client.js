/* Front end client to WAMP Router*/

var connection = new autobahn.Connection({
                                            url: 'ws://127.0.0.1:8080/ws',
                                            realm: "crossbardemo"
                                        });

var SESSION = null;

const ALERT_DANGER = "alert-danger"
const ALERT_SUCCES = "alert-success"

var engineStatus = 0;
var afterStart = 0;

var maxTemp = 100000;

connection.onopen = async function (session) 
{
    SESSION = session;
    console.log("Connected to Crossbar.io on '" + session.realm + "'");

    // Suscribe to views topic
    session.subscribe("com.myapp.request_served", updateViews)
    session.subscribe("com.myapp.engine1.temp", updateTemp)
    session.subscribe("com.myapp.engine1.fan_status", updateFan)
    session.subscribe('com.myapp.engine1.max_temp', updateMaxTemp)

    await session.call("com.myapp.engine1.get_fan_status")
    await session.call("com.myapp.engine1.get_max_temp")

    afterStart = 1;
}

// Updates views
function updateViews(args) {
    
    $ ("#views").text(args[0])
}

function updateTemp(args) {

    let temp = args[0].toFixed(2)


    // Remuevo clases del borde del estado del motor
    $ ("#motor-border-test").removeClass("border-left-success").removeClass("border-left-danger")
    
    // Remuevo clases de la tarjeta de estado de motor
    $ ("#card-color-test").removeClass("bg-success").removeClass("bg-danger")

    // Remuevo clase del texto de motor

    $ ("#motor-test").removeClass("text-success").removeClass("text-danger")
    

    if ( temp > maxTemp){

        // Cambiar el color del borde del bloque de temperatura y el boton de texto a PELIGRO

        $ ("#motor-border-test").addClass("border-left-danger")

        $ ("#motor-test").addClass("text-danger")

        // Cambiar color y texto de la tarjeta a PELIGRO

        $ ("#card-color-test").addClass("bg-danger")
        
        $ ("#card-text-test").text("El motor está sobrecalentado.")

        if (!engineStatus) {
            sendAlert(ALERT_DANGER, "El motor está sobrecalentando")
            engineStatus = !engineStatus;
        }

    }
    else{

        // Cambiar el color del borde del bloque de temperatura y el color de texto a OK

        $ ("#motor-border-test").addClass("border-left-success")

        $ ("#motor-test").addClass("text-success")

        // Cambiar color y texto de la tarjeta a OK

        $ ("#card-color-test").addClass("bg-success")

        $ ("#card-text-test").text("El motor está OK.")

        if (engineStatus) {
            sendAlert(ALERT_SUCCES, "El motor entro en zona óptima")
            engineStatus = !engineStatus;
        }
    }

    $ ("#motor-temp").text(temp)
    $ ("#motor-temp-test").text(temp)

}

function updateFan(args) {

    $ ("#motor-fan").text(args[0] ? ("Encendido") : ("Apagado"))

    $ ("#motor-fan-test").text(args[0] ? ("Encendido") : ("Apagado"))

    // Borro clases

    $ ("#fan-btn-test").removeClass("btn-success").removeClass("btn-warning")

    if (args[0]){

        // Cambiar texto y color del boton en UPDATE FAN

        $ ("#fan-btn-test").addClass("btn-warning")

        $ ("#fan-btn-text-test").text("Ventilador encendido")

    }
    else{

        // Cambiar texto y color del boton en UPDATE FAN

        $ ("#fan-btn-test").addClass("btn-success")

        $ ("#fan-btn-text-test").text("Encender ventilador")

        if (afterStart)
            sendAlert("alert-info", "El ventilador fue apagado")

    }
}

$ ("#fan-btn-test").click(async function() {
    await startFan()
})

async function startFan() {
    sendAlert(ALERT_SUCCES, "El ventilador fue accionado")
    await SESSION.call("com.myapp.engine1.start_fan")
    return;
}

function sendAlert(type, text)
{
    let id = Date.now()

    let alertHolder = document.getElementById("alert-holer");
    alertHolder.innerHTML += `<div id="${id}" class="alert ${type}">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                ${text}
                            </div>`

    setTimeout( () => {
       $ (`#${id}`).remove();
    }, 5000)
}

function updateMaxTemp(args) {
    
    if (maxTemp !== args[0]) {
        sendAlert("alert-info", `La temperatura de alarma fue cambiada a ${maxTemp} °C`);
    }

    maxTemp = args[0]
} 

async function setMaxTemp(temp) {
    await SESSION.call("com.myapp.engine1.set_max_temp", [temp])
}

async function setMinTemp(temp) {
    await SESSION.call("com.myapp.engine1.set_min_temp", [temp])
}

async function getFanStatus() {
    await session.call("com.myapp.engine1.get_fan_status")
}

connection.open()