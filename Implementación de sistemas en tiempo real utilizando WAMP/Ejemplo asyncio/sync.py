import time

import asyncio
import aiohttp

# Definimos una funcion extra para gestionar las tareas
async def get(url) -> None:

    print('[SEND] get {}'.format(url))
    start_time = time.time()

    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            print("[RESPONSE] {}".format(url), 'Status code:', resp.status, 'Delay: {:f} s'.format(time.time() - start_time))

# Registramos una corutina o funcion asincrona
async def main():
    
    print('EJ: Tareas sincronas')
    start_time = time.time()

    # En este caso esperamos a cada respuesta.
    await get('https://www.ing.unrc.edu.ar') # Esperamos a la primera
    await get('https://www.reddit.com/r/python') # Esperamos a la segunda
    await get('https://www.youtube.com/') # Tercera

    # Vemos que este caso es similar al caso sincrono, dado que cada tarea asíncrona
    # es esperada por separado antes de seguir con la ejecución

    print('Total delay: {:f} s'.format(time.time() - start_time))
    
if __name__ == "__main__":
    asyncio.run(main())