# Contenido del ejemplo

Este ejemplo busca mostrar la diferencia entre tareas asíncronas concurrentes y tareas síncronas. 

Son requeridas las liberias de python

* <code>asyncio</code>
* <code>aiohttp</code>

Este ejemplo consta de dos archivos:
* <code>gather.py</code> el cual contiene 3 tareas asíncronas que son ejecutadas de forma paralela. 
* <code>sync.py</code> el cual presenta las mismas 3 tareas pero ejecutadas de forma síncrona. 

*Es recomendable correr cualquiera de los dos ejemplos de antes de realizar la comparación para evitar tiempos de consulta DNS.*

*Destacar que en el caso de <code>sync.py</code> se utiliza asyncio emulando tareas sincronas para poder comparar, esto debido al funcionamiento de las solicitudes de la liberia <code>aiohttp</code>.*