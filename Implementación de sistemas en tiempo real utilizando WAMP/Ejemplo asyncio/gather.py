import time

import asyncio
import aiohttp

# Definimos una funcion extra para gestionar las tareas
async def get(url) -> None:

    print('[SEND] get {}'.format(url))
    start_time = time.time()

    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            print("[RESPONSE] {}".format(url), 'Status code:', resp.status, 'Delay: {:f} s'.format(time.time() - start_time))

# Registramos una corutina o funcion asincrona
async def main():
    
    print('EJ: Tareas asíncronas paralelas')
    start_time = time.time()

    # La funcion gather permite paralelizar las corutinas
    await asyncio.gather(
        get('https://www.ing.unrc.edu.ar'), 
        get('https://www.reddit.com/r/python'),
        get('https://www.youtube.com/')
    )

    print('Total delay: {:f} s'.format(time.time() - start_time))

    # En este ejemplo vemos que ambas tareas son disparadas al mismo tiempo, por lo que
    # la ejecución dura aproximadamente lo mismo que la tarea mas lenta.
    
if __name__ == "__main__":
    asyncio.run(main())